﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Directory
{
    public partial class btnCall : System.Windows.Forms.Form
    {
        MyDb obj = new MyDb();
        string userName;
        public btnCall()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            loadContacts();
      
        }
        
        public void User(string user)
        {
            this.userName = user;
        }
        
        public void loadContacts()
        {
            Grid.DataSource = obj.getContacts(userName);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form3 obj = new Form3();
            obj.User(userName);
            obj.ShowDialog();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = Grid.DataSource;
            bs.Filter = "[Company] like '%" + txtSearch.Text + "%'";
            Grid.DataSource = bs;
        }

        public void displayContacts(string company)
        {
            Form3 obj = new Form3();
            obj.display(company);
            obj.User(userName);
            obj.ShowDialog();
        }

        private void btnRe_Click(object sender, EventArgs e)
        {
            loadContacts();
        }
        
        private void Grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string company = Grid.Rows[e.RowIndex].Cells[0].Value.ToString();
            displayContacts(company);
        }

        private void Grid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                string company = Grid.Rows[e.RowIndex].Cells[0].Value.ToString();
                string phoneNo = Grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Trim();
                int contact;
                DataTable cT = obj.GetDataTable("Select * from ContactsTable where Company='" + company + "' ");
                if (cT.Rows.Count != 0)
                {
                    if (cT.Rows[0][3].ToString().Trim() == phoneNo)
                    {
                        contact = 3;
                    }
                    else if (cT.Rows[0][7].ToString().Trim() == phoneNo)
                    {
                        contact = 7;
                    }
                    else if (cT.Rows[0][11].ToString().Trim() == phoneNo)
                    {
                        contact = 11;
                    }
                    else if (cT.Rows[0][15].ToString().Trim() == phoneNo)
                    {
                        contact = 15;
                    }
                    else if (cT.Rows[0][19].ToString().Trim() == phoneNo)
                    {
                        contact = 19;
                    }
                    else if (cT.Rows[0][23].ToString().Trim() == phoneNo)
                    {
                        contact = 23;
                    }
                    else if (cT.Rows[0][27].ToString().Trim() == phoneNo)
                    {
                        contact = 27;
                    }
                    else if (cT.Rows[0][31].ToString().Trim() == phoneNo)
                    {
                        contact = 31;
                    }
                    else if (cT.Rows[0][35].ToString().Trim() == phoneNo)
                    {
                        contact = 35;
                    }
                    else if (cT.Rows[0][39].ToString().Trim() == phoneNo)
                    {
                        contact = 39;
                    }
                    else
                    {
                        contact = 0;
                    }

                    if (contact != 0)
                    {
                        string name = cT.Rows[0][contact - 2].ToString().Trim();
                        string pos = cT.Rows[0][contact - 1].ToString().Trim();

                        if(name!="" && phoneNo != "")
                        {
                            Form4 obj = new Form4();
                            obj.display(name, pos, phoneNo);
                            obj.ShowDialog();
                        }
                    }

                }

            }
            catch(Exception ex)
            {

            }
              
        }
    }
}
