﻿namespace Directory
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPhoneNo1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtName1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPhoneNo2 = new System.Windows.Forms.TextBox();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPhoneNo3 = new System.Windows.Forms.TextBox();
            this.txtName3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPhoneNo4 = new System.Windows.Forms.TextBox();
            this.txtName4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhoneNo5 = new System.Windows.Forms.TextBox();
            this.txtName5 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPhoneNo6 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPhoneNo7 = new System.Windows.Forms.TextBox();
            this.txtName7 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPhoneNo8 = new System.Windows.Forms.TextBox();
            this.txtName8 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPhoneNo9 = new System.Windows.Forms.TextBox();
            this.txtName9 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPhoneNo10 = new System.Windows.Forms.TextBox();
            this.txtName10 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtName6 = new System.Windows.Forms.TextBox();
            this.rbtnPublic = new System.Windows.Forms.RadioButton();
            this.rbtnPersonal = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDes10 = new System.Windows.Forms.TextBox();
            this.txtDes9 = new System.Windows.Forms.TextBox();
            this.txtDes8 = new System.Windows.Forms.TextBox();
            this.txtDes7 = new System.Windows.Forms.TextBox();
            this.txtDes6 = new System.Windows.Forms.TextBox();
            this.txtDes5 = new System.Windows.Forms.TextBox();
            this.txtDes4 = new System.Windows.Forms.TextBox();
            this.txtDes3 = new System.Windows.Forms.TextBox();
            this.txtDes2 = new System.Windows.Forms.TextBox();
            this.txtDes1 = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtEmail10 = new System.Windows.Forms.TextBox();
            this.txtEmail9 = new System.Windows.Forms.TextBox();
            this.txtEmail8 = new System.Windows.Forms.TextBox();
            this.txtEmail7 = new System.Windows.Forms.TextBox();
            this.txtEmail6 = new System.Windows.Forms.TextBox();
            this.txtEmail5 = new System.Windows.Forms.TextBox();
            this.txtEmail4 = new System.Windows.Forms.TextBox();
            this.txtEmail3 = new System.Windows.Forms.TextBox();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.txtEmail1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCall1 = new System.Windows.Forms.Button();
            this.btnCall2 = new System.Windows.Forms.Button();
            this.btnCall3 = new System.Windows.Forms.Button();
            this.btnCall4 = new System.Windows.Forms.Button();
            this.btnCall5 = new System.Windows.Forms.Button();
            this.btnCall6 = new System.Windows.Forms.Button();
            this.btnCall7 = new System.Windows.Forms.Button();
            this.btnCall8 = new System.Windows.Forms.Button();
            this.btnCall9 = new System.Windows.Forms.Button();
            this.btnCall10 = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.Color.LightGray;
            this.txtCompany.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompany.Location = new System.Drawing.Point(157, 81);
            this.txtCompany.Multiline = true;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(99, 20);
            this.txtCompany.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Company";
            // 
            // txtPhoneNo1
            // 
            this.txtPhoneNo1.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo1.Location = new System.Drawing.Point(399, 143);
            this.txtPhoneNo1.Multiline = true;
            this.txtPhoneNo1.Name = "txtPhoneNo1";
            this.txtPhoneNo1.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo1.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(60, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Contact 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(60, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Contact 2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(60, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 17);
            this.label10.TabIndex = 2;
            this.label10.Text = "Contact 3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(60, 253);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Contact 4";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(60, 289);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "Contact 5";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(60, 325);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 17);
            this.label19.TabIndex = 2;
            this.label19.Text = "Contact 6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(60, 361);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Contact 7";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(60, 398);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "Contact 8";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(60, 434);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 17);
            this.label28.TabIndex = 2;
            this.label28.Text = "Contact 9";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(60, 469);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 17);
            this.label31.TabIndex = 2;
            this.label31.Text = "Contact 10";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(543, 523);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 36);
            this.btnAdd.TabIndex = 42;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtName1
            // 
            this.txtName1.BackColor = System.Drawing.Color.LightGray;
            this.txtName1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName1.Location = new System.Drawing.Point(157, 143);
            this.txtName1.Multiline = true;
            this.txtName1.Name = "txtName1";
            this.txtName1.Size = new System.Drawing.Size(99, 20);
            this.txtName1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // txtPhoneNo2
            // 
            this.txtPhoneNo2.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo2.Location = new System.Drawing.Point(399, 182);
            this.txtPhoneNo2.Multiline = true;
            this.txtPhoneNo2.Name = "txtPhoneNo2";
            this.txtPhoneNo2.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo2.TabIndex = 8;
            // 
            // txtName2
            // 
            this.txtName2.BackColor = System.Drawing.Color.LightGray;
            this.txtName2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName2.Location = new System.Drawing.Point(157, 182);
            this.txtName2.Multiline = true;
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(99, 20);
            this.txtName2.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(144, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 2;
            // 
            // txtPhoneNo3
            // 
            this.txtPhoneNo3.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo3.Location = new System.Drawing.Point(399, 218);
            this.txtPhoneNo3.Multiline = true;
            this.txtPhoneNo3.Name = "txtPhoneNo3";
            this.txtPhoneNo3.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo3.TabIndex = 12;
            // 
            // txtName3
            // 
            this.txtName3.BackColor = System.Drawing.Color.LightGray;
            this.txtName3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName3.Location = new System.Drawing.Point(157, 218);
            this.txtName3.Multiline = true;
            this.txtName3.Name = "txtName3";
            this.txtName3.Size = new System.Drawing.Size(99, 20);
            this.txtName3.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(144, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 2;
            // 
            // txtPhoneNo4
            // 
            this.txtPhoneNo4.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo4.Location = new System.Drawing.Point(399, 253);
            this.txtPhoneNo4.Multiline = true;
            this.txtPhoneNo4.Name = "txtPhoneNo4";
            this.txtPhoneNo4.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo4.TabIndex = 16;
            // 
            // txtName4
            // 
            this.txtName4.BackColor = System.Drawing.Color.LightGray;
            this.txtName4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName4.Location = new System.Drawing.Point(157, 253);
            this.txtName4.Multiline = true;
            this.txtName4.Name = "txtName4";
            this.txtName4.Size = new System.Drawing.Size(99, 20);
            this.txtName4.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(144, 257);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 2;
            // 
            // txtPhoneNo5
            // 
            this.txtPhoneNo5.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo5.Location = new System.Drawing.Point(399, 291);
            this.txtPhoneNo5.Multiline = true;
            this.txtPhoneNo5.Name = "txtPhoneNo5";
            this.txtPhoneNo5.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo5.TabIndex = 20;
            // 
            // txtName5
            // 
            this.txtName5.BackColor = System.Drawing.Color.LightGray;
            this.txtName5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName5.Location = new System.Drawing.Point(157, 291);
            this.txtName5.Multiline = true;
            this.txtName5.Name = "txtName5";
            this.txtName5.Size = new System.Drawing.Size(99, 20);
            this.txtName5.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(144, 291);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 2;
            // 
            // txtPhoneNo6
            // 
            this.txtPhoneNo6.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo6.Location = new System.Drawing.Point(399, 328);
            this.txtPhoneNo6.Multiline = true;
            this.txtPhoneNo6.Name = "txtPhoneNo6";
            this.txtPhoneNo6.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo6.TabIndex = 24;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(144, 328);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 2;
            // 
            // txtPhoneNo7
            // 
            this.txtPhoneNo7.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo7.Location = new System.Drawing.Point(399, 361);
            this.txtPhoneNo7.Multiline = true;
            this.txtPhoneNo7.Name = "txtPhoneNo7";
            this.txtPhoneNo7.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo7.TabIndex = 28;
            // 
            // txtName7
            // 
            this.txtName7.BackColor = System.Drawing.Color.LightGray;
            this.txtName7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName7.Location = new System.Drawing.Point(157, 361);
            this.txtName7.Multiline = true;
            this.txtName7.Name = "txtName7";
            this.txtName7.Size = new System.Drawing.Size(99, 20);
            this.txtName7.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(144, 364);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 2;
            // 
            // txtPhoneNo8
            // 
            this.txtPhoneNo8.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo8.Location = new System.Drawing.Point(399, 398);
            this.txtPhoneNo8.Multiline = true;
            this.txtPhoneNo8.Name = "txtPhoneNo8";
            this.txtPhoneNo8.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo8.TabIndex = 32;
            // 
            // txtName8
            // 
            this.txtName8.BackColor = System.Drawing.Color.LightGray;
            this.txtName8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName8.Location = new System.Drawing.Point(157, 398);
            this.txtName8.Multiline = true;
            this.txtName8.Name = "txtName8";
            this.txtName8.Size = new System.Drawing.Size(99, 20);
            this.txtName8.TabIndex = 30;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(144, 400);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(0, 13);
            this.label24.TabIndex = 2;
            // 
            // txtPhoneNo9
            // 
            this.txtPhoneNo9.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo9.Location = new System.Drawing.Point(399, 434);
            this.txtPhoneNo9.Multiline = true;
            this.txtPhoneNo9.Name = "txtPhoneNo9";
            this.txtPhoneNo9.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo9.TabIndex = 36;
            // 
            // txtName9
            // 
            this.txtName9.BackColor = System.Drawing.Color.LightGray;
            this.txtName9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName9.Location = new System.Drawing.Point(157, 434);
            this.txtName9.Multiline = true;
            this.txtName9.Name = "txtName9";
            this.txtName9.Size = new System.Drawing.Size(99, 20);
            this.txtName9.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(144, 437);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 13);
            this.label27.TabIndex = 2;
            // 
            // txtPhoneNo10
            // 
            this.txtPhoneNo10.BackColor = System.Drawing.Color.LightGray;
            this.txtPhoneNo10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPhoneNo10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo10.Location = new System.Drawing.Point(399, 469);
            this.txtPhoneNo10.Multiline = true;
            this.txtPhoneNo10.Name = "txtPhoneNo10";
            this.txtPhoneNo10.Size = new System.Drawing.Size(99, 20);
            this.txtPhoneNo10.TabIndex = 40;
            // 
            // txtName10
            // 
            this.txtName10.BackColor = System.Drawing.Color.LightGray;
            this.txtName10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName10.Location = new System.Drawing.Point(157, 469);
            this.txtName10.Multiline = true;
            this.txtName10.Name = "txtName10";
            this.txtName10.Size = new System.Drawing.Size(99, 20);
            this.txtName10.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(144, 471);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(0, 13);
            this.label30.TabIndex = 2;
            // 
            // txtName6
            // 
            this.txtName6.BackColor = System.Drawing.Color.LightGray;
            this.txtName6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName6.Location = new System.Drawing.Point(157, 328);
            this.txtName6.Multiline = true;
            this.txtName6.Name = "txtName6";
            this.txtName6.Size = new System.Drawing.Size(99, 20);
            this.txtName6.TabIndex = 22;
            // 
            // rbtnPublic
            // 
            this.rbtnPublic.AutoSize = true;
            this.rbtnPublic.FlatAppearance.BorderColor = System.Drawing.Color.DeepPink;
            this.rbtnPublic.Location = new System.Drawing.Point(33, 26);
            this.rbtnPublic.Name = "rbtnPublic";
            this.rbtnPublic.Size = new System.Drawing.Size(54, 17);
            this.rbtnPublic.TabIndex = 1;
            this.rbtnPublic.TabStop = true;
            this.rbtnPublic.Text = "Public";
            this.rbtnPublic.UseVisualStyleBackColor = true;
            // 
            // rbtnPersonal
            // 
            this.rbtnPersonal.AutoSize = true;
            this.rbtnPersonal.FlatAppearance.BorderColor = System.Drawing.Color.DeepPink;
            this.rbtnPersonal.Location = new System.Drawing.Point(123, 26);
            this.rbtnPersonal.Name = "rbtnPersonal";
            this.rbtnPersonal.Size = new System.Drawing.Size(66, 17);
            this.rbtnPersonal.TabIndex = 1;
            this.rbtnPersonal.TabStop = true;
            this.rbtnPersonal.Text = "Personal";
            this.rbtnPersonal.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(170, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(281, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Designation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(416, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Phone No.";
            // 
            // txtDes10
            // 
            this.txtDes10.BackColor = System.Drawing.Color.LightGray;
            this.txtDes10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes10.Location = new System.Drawing.Point(276, 469);
            this.txtDes10.Multiline = true;
            this.txtDes10.Name = "txtDes10";
            this.txtDes10.Size = new System.Drawing.Size(99, 20);
            this.txtDes10.TabIndex = 39;
            // 
            // txtDes9
            // 
            this.txtDes9.BackColor = System.Drawing.Color.LightGray;
            this.txtDes9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes9.Location = new System.Drawing.Point(276, 434);
            this.txtDes9.Multiline = true;
            this.txtDes9.Name = "txtDes9";
            this.txtDes9.Size = new System.Drawing.Size(99, 20);
            this.txtDes9.TabIndex = 35;
            // 
            // txtDes8
            // 
            this.txtDes8.BackColor = System.Drawing.Color.LightGray;
            this.txtDes8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes8.Location = new System.Drawing.Point(276, 398);
            this.txtDes8.Multiline = true;
            this.txtDes8.Name = "txtDes8";
            this.txtDes8.Size = new System.Drawing.Size(99, 20);
            this.txtDes8.TabIndex = 31;
            // 
            // txtDes7
            // 
            this.txtDes7.BackColor = System.Drawing.Color.LightGray;
            this.txtDes7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes7.Location = new System.Drawing.Point(276, 361);
            this.txtDes7.Multiline = true;
            this.txtDes7.Name = "txtDes7";
            this.txtDes7.Size = new System.Drawing.Size(99, 20);
            this.txtDes7.TabIndex = 27;
            // 
            // txtDes6
            // 
            this.txtDes6.BackColor = System.Drawing.Color.LightGray;
            this.txtDes6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes6.Location = new System.Drawing.Point(276, 328);
            this.txtDes6.Multiline = true;
            this.txtDes6.Name = "txtDes6";
            this.txtDes6.Size = new System.Drawing.Size(99, 20);
            this.txtDes6.TabIndex = 23;
            // 
            // txtDes5
            // 
            this.txtDes5.BackColor = System.Drawing.Color.LightGray;
            this.txtDes5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes5.Location = new System.Drawing.Point(276, 291);
            this.txtDes5.Multiline = true;
            this.txtDes5.Name = "txtDes5";
            this.txtDes5.Size = new System.Drawing.Size(99, 20);
            this.txtDes5.TabIndex = 19;
            // 
            // txtDes4
            // 
            this.txtDes4.BackColor = System.Drawing.Color.LightGray;
            this.txtDes4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes4.Location = new System.Drawing.Point(276, 253);
            this.txtDes4.Multiline = true;
            this.txtDes4.Name = "txtDes4";
            this.txtDes4.Size = new System.Drawing.Size(99, 20);
            this.txtDes4.TabIndex = 15;
            // 
            // txtDes3
            // 
            this.txtDes3.BackColor = System.Drawing.Color.LightGray;
            this.txtDes3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes3.Location = new System.Drawing.Point(276, 218);
            this.txtDes3.Multiline = true;
            this.txtDes3.Name = "txtDes3";
            this.txtDes3.Size = new System.Drawing.Size(99, 20);
            this.txtDes3.TabIndex = 11;
            // 
            // txtDes2
            // 
            this.txtDes2.BackColor = System.Drawing.Color.LightGray;
            this.txtDes2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes2.Location = new System.Drawing.Point(276, 182);
            this.txtDes2.Multiline = true;
            this.txtDes2.Name = "txtDes2";
            this.txtDes2.Size = new System.Drawing.Size(99, 20);
            this.txtDes2.TabIndex = 7;
            // 
            // txtDes1
            // 
            this.txtDes1.BackColor = System.Drawing.Color.LightGray;
            this.txtDes1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDes1.Location = new System.Drawing.Point(276, 143);
            this.txtDes1.Multiline = true;
            this.txtDes1.Name = "txtDes1";
            this.txtDes1.Size = new System.Drawing.Size(99, 20);
            this.txtDes1.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(559, 523);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 36);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtEmail10
            // 
            this.txtEmail10.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail10.Location = new System.Drawing.Point(519, 469);
            this.txtEmail10.Multiline = true;
            this.txtEmail10.Name = "txtEmail10";
            this.txtEmail10.Size = new System.Drawing.Size(99, 20);
            this.txtEmail10.TabIndex = 41;
            // 
            // txtEmail9
            // 
            this.txtEmail9.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail9.Location = new System.Drawing.Point(519, 434);
            this.txtEmail9.Multiline = true;
            this.txtEmail9.Name = "txtEmail9";
            this.txtEmail9.Size = new System.Drawing.Size(99, 20);
            this.txtEmail9.TabIndex = 37;
            // 
            // txtEmail8
            // 
            this.txtEmail8.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail8.Location = new System.Drawing.Point(519, 398);
            this.txtEmail8.Multiline = true;
            this.txtEmail8.Name = "txtEmail8";
            this.txtEmail8.Size = new System.Drawing.Size(99, 20);
            this.txtEmail8.TabIndex = 33;
            // 
            // txtEmail7
            // 
            this.txtEmail7.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail7.Location = new System.Drawing.Point(519, 361);
            this.txtEmail7.Multiline = true;
            this.txtEmail7.Name = "txtEmail7";
            this.txtEmail7.Size = new System.Drawing.Size(99, 20);
            this.txtEmail7.TabIndex = 29;
            // 
            // txtEmail6
            // 
            this.txtEmail6.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail6.Location = new System.Drawing.Point(519, 328);
            this.txtEmail6.Multiline = true;
            this.txtEmail6.Name = "txtEmail6";
            this.txtEmail6.Size = new System.Drawing.Size(99, 20);
            this.txtEmail6.TabIndex = 25;
            // 
            // txtEmail5
            // 
            this.txtEmail5.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail5.Location = new System.Drawing.Point(519, 291);
            this.txtEmail5.Multiline = true;
            this.txtEmail5.Name = "txtEmail5";
            this.txtEmail5.Size = new System.Drawing.Size(99, 20);
            this.txtEmail5.TabIndex = 21;
            // 
            // txtEmail4
            // 
            this.txtEmail4.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail4.Location = new System.Drawing.Point(519, 253);
            this.txtEmail4.Multiline = true;
            this.txtEmail4.Name = "txtEmail4";
            this.txtEmail4.Size = new System.Drawing.Size(99, 20);
            this.txtEmail4.TabIndex = 17;
            // 
            // txtEmail3
            // 
            this.txtEmail3.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail3.Location = new System.Drawing.Point(519, 218);
            this.txtEmail3.Multiline = true;
            this.txtEmail3.Name = "txtEmail3";
            this.txtEmail3.Size = new System.Drawing.Size(99, 20);
            this.txtEmail3.TabIndex = 13;
            // 
            // txtEmail2
            // 
            this.txtEmail2.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail2.Location = new System.Drawing.Point(519, 182);
            this.txtEmail2.Multiline = true;
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(99, 20);
            this.txtEmail2.TabIndex = 9;
            // 
            // txtEmail1
            // 
            this.txtEmail1.BackColor = System.Drawing.Color.LightGray;
            this.txtEmail1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmail1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail1.Location = new System.Drawing.Point(519, 143);
            this.txtEmail1.Multiline = true;
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(99, 20);
            this.txtEmail1.TabIndex = 5;
            this.txtEmail1.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(540, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Email";
            // 
            // btnCall1
            // 
            this.btnCall1.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall1.ForeColor = System.Drawing.Color.White;
            this.btnCall1.Location = new System.Drawing.Point(639, 143);
            this.btnCall1.Name = "btnCall1";
            this.btnCall1.Size = new System.Drawing.Size(35, 23);
            this.btnCall1.TabIndex = 26;
            this.btnCall1.Text = "Call";
            this.btnCall1.UseVisualStyleBackColor = false;
            this.btnCall1.Visible = false;
            // 
            // btnCall2
            // 
            this.btnCall2.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall2.ForeColor = System.Drawing.Color.White;
            this.btnCall2.Location = new System.Drawing.Point(639, 182);
            this.btnCall2.Name = "btnCall2";
            this.btnCall2.Size = new System.Drawing.Size(35, 23);
            this.btnCall2.TabIndex = 26;
            this.btnCall2.Text = "Call";
            this.btnCall2.UseVisualStyleBackColor = false;
            this.btnCall2.Visible = false;
            // 
            // btnCall3
            // 
            this.btnCall3.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall3.ForeColor = System.Drawing.Color.White;
            this.btnCall3.Location = new System.Drawing.Point(639, 218);
            this.btnCall3.Name = "btnCall3";
            this.btnCall3.Size = new System.Drawing.Size(35, 23);
            this.btnCall3.TabIndex = 26;
            this.btnCall3.Text = "Call";
            this.btnCall3.UseVisualStyleBackColor = false;
            this.btnCall3.Visible = false;
            // 
            // btnCall4
            // 
            this.btnCall4.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall4.ForeColor = System.Drawing.Color.White;
            this.btnCall4.Location = new System.Drawing.Point(639, 253);
            this.btnCall4.Name = "btnCall4";
            this.btnCall4.Size = new System.Drawing.Size(35, 23);
            this.btnCall4.TabIndex = 26;
            this.btnCall4.Text = "Call";
            this.btnCall4.UseVisualStyleBackColor = false;
            this.btnCall4.Visible = false;
            // 
            // btnCall5
            // 
            this.btnCall5.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall5.ForeColor = System.Drawing.Color.White;
            this.btnCall5.Location = new System.Drawing.Point(639, 291);
            this.btnCall5.Name = "btnCall5";
            this.btnCall5.Size = new System.Drawing.Size(35, 23);
            this.btnCall5.TabIndex = 26;
            this.btnCall5.Text = "Call";
            this.btnCall5.UseVisualStyleBackColor = false;
            this.btnCall5.Visible = false;
            // 
            // btnCall6
            // 
            this.btnCall6.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall6.ForeColor = System.Drawing.Color.White;
            this.btnCall6.Location = new System.Drawing.Point(639, 328);
            this.btnCall6.Name = "btnCall6";
            this.btnCall6.Size = new System.Drawing.Size(35, 23);
            this.btnCall6.TabIndex = 26;
            this.btnCall6.Text = "Call";
            this.btnCall6.UseVisualStyleBackColor = false;
            this.btnCall6.Visible = false;
            // 
            // btnCall7
            // 
            this.btnCall7.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall7.ForeColor = System.Drawing.Color.White;
            this.btnCall7.Location = new System.Drawing.Point(639, 361);
            this.btnCall7.Name = "btnCall7";
            this.btnCall7.Size = new System.Drawing.Size(35, 23);
            this.btnCall7.TabIndex = 26;
            this.btnCall7.Text = "Call";
            this.btnCall7.UseVisualStyleBackColor = false;
            this.btnCall7.Visible = false;
            // 
            // btnCall8
            // 
            this.btnCall8.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall8.ForeColor = System.Drawing.Color.White;
            this.btnCall8.Location = new System.Drawing.Point(639, 398);
            this.btnCall8.Name = "btnCall8";
            this.btnCall8.Size = new System.Drawing.Size(35, 23);
            this.btnCall8.TabIndex = 26;
            this.btnCall8.Text = "Call";
            this.btnCall8.UseVisualStyleBackColor = false;
            this.btnCall8.Visible = false;
            // 
            // btnCall9
            // 
            this.btnCall9.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall9.ForeColor = System.Drawing.Color.White;
            this.btnCall9.Location = new System.Drawing.Point(639, 434);
            this.btnCall9.Name = "btnCall9";
            this.btnCall9.Size = new System.Drawing.Size(35, 23);
            this.btnCall9.TabIndex = 26;
            this.btnCall9.Text = "Call";
            this.btnCall9.UseVisualStyleBackColor = false;
            this.btnCall9.Visible = false;
            // 
            // btnCall10
            // 
            this.btnCall10.BackColor = System.Drawing.Color.DeepPink;
            this.btnCall10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCall10.ForeColor = System.Drawing.Color.White;
            this.btnCall10.Location = new System.Drawing.Point(639, 469);
            this.btnCall10.Name = "btnCall10";
            this.btnCall10.Size = new System.Drawing.Size(35, 23);
            this.btnCall10.TabIndex = 26;
            this.btnCall10.Text = "Call";
            this.btnCall10.UseVisualStyleBackColor = false;
            this.btnCall10.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.MediumVioletRed;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(625, 523);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 36);
            this.btnDelete.TabIndex = 42;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(765, 571);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCall10);
            this.Controls.Add(this.btnCall9);
            this.Controls.Add(this.btnCall8);
            this.Controls.Add(this.btnCall7);
            this.Controls.Add(this.btnCall6);
            this.Controls.Add(this.btnCall5);
            this.Controls.Add(this.btnCall4);
            this.Controls.Add(this.btnCall3);
            this.Controls.Add(this.btnCall2);
            this.Controls.Add(this.btnCall1);
            this.Controls.Add(this.txtEmail10);
            this.Controls.Add(this.txtEmail9);
            this.Controls.Add(this.txtEmail8);
            this.Controls.Add(this.txtEmail7);
            this.Controls.Add(this.txtEmail6);
            this.Controls.Add(this.txtEmail5);
            this.Controls.Add(this.txtEmail4);
            this.Controls.Add(this.txtEmail3);
            this.Controls.Add(this.txtEmail2);
            this.Controls.Add(this.txtEmail1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtDes10);
            this.Controls.Add(this.txtDes9);
            this.Controls.Add(this.txtDes8);
            this.Controls.Add(this.txtDes7);
            this.Controls.Add(this.txtDes6);
            this.Controls.Add(this.txtDes5);
            this.Controls.Add(this.txtDes4);
            this.Controls.Add(this.txtDes3);
            this.Controls.Add(this.txtDes2);
            this.Controls.Add(this.txtDes1);
            this.Controls.Add(this.rbtnPersonal);
            this.Controls.Add(this.rbtnPublic);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtName10);
            this.Controls.Add(this.txtName9);
            this.Controls.Add(this.txtName8);
            this.Controls.Add(this.txtName7);
            this.Controls.Add(this.txtName6);
            this.Controls.Add(this.txtName5);
            this.Controls.Add(this.txtName4);
            this.Controls.Add(this.txtName3);
            this.Controls.Add(this.txtName2);
            this.Controls.Add(this.txtName1);
            this.Controls.Add(this.txtPhoneNo10);
            this.Controls.Add(this.txtPhoneNo9);
            this.Controls.Add(this.txtPhoneNo8);
            this.Controls.Add(this.txtPhoneNo7);
            this.Controls.Add(this.txtPhoneNo6);
            this.Controls.Add(this.txtPhoneNo5);
            this.Controls.Add(this.txtPhoneNo4);
            this.Controls.Add(this.txtPhoneNo3);
            this.Controls.Add(this.txtPhoneNo2);
            this.Controls.Add(this.txtPhoneNo1);
            this.Controls.Add(this.txtCompany);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.Text = "Add Contacts";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPhoneNo1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtName1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPhoneNo2;
        private System.Windows.Forms.TextBox txtName2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPhoneNo3;
        private System.Windows.Forms.TextBox txtName3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPhoneNo4;
        private System.Windows.Forms.TextBox txtName4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPhoneNo5;
        private System.Windows.Forms.TextBox txtName5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPhoneNo6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPhoneNo7;
        private System.Windows.Forms.TextBox txtName7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPhoneNo8;
        private System.Windows.Forms.TextBox txtName8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtPhoneNo9;
        private System.Windows.Forms.TextBox txtName9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPhoneNo10;
        private System.Windows.Forms.TextBox txtName10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtName6;
        private System.Windows.Forms.RadioButton rbtnPublic;
        private System.Windows.Forms.RadioButton rbtnPersonal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDes10;
        private System.Windows.Forms.TextBox txtDes9;
        private System.Windows.Forms.TextBox txtDes8;
        private System.Windows.Forms.TextBox txtDes7;
        private System.Windows.Forms.TextBox txtDes6;
        private System.Windows.Forms.TextBox txtDes5;
        private System.Windows.Forms.TextBox txtDes4;
        private System.Windows.Forms.TextBox txtDes3;
        private System.Windows.Forms.TextBox txtDes2;
        private System.Windows.Forms.TextBox txtDes1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtEmail10;
        private System.Windows.Forms.TextBox txtEmail9;
        private System.Windows.Forms.TextBox txtEmail8;
        private System.Windows.Forms.TextBox txtEmail7;
        private System.Windows.Forms.TextBox txtEmail6;
        private System.Windows.Forms.TextBox txtEmail5;
        private System.Windows.Forms.TextBox txtEmail4;
        private System.Windows.Forms.TextBox txtEmail3;
        private System.Windows.Forms.TextBox txtEmail2;
        private System.Windows.Forms.TextBox txtEmail1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCall1;
        private System.Windows.Forms.Button btnCall2;
        private System.Windows.Forms.Button btnCall3;
        private System.Windows.Forms.Button btnCall4;
        private System.Windows.Forms.Button btnCall5;
        private System.Windows.Forms.Button btnCall6;
        private System.Windows.Forms.Button btnCall7;
        private System.Windows.Forms.Button btnCall8;
        private System.Windows.Forms.Button btnCall9;
        private System.Windows.Forms.Button btnCall10;
        private System.Windows.Forms.Button btnDelete;
    }
}