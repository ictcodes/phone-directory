﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Directory
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        public void display(string name, string pos, string phoneNo)
        {
            lblName.Text = name;
            lblPos.Text = pos;
            lblPhone.Text = phoneNo;
        }
    }
}
