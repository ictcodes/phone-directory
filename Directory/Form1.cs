﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Directory
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        MyDb obj = new MyDb();
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if(obj.login(txtUserName.Text, txtPassword.Text))
            {
                btnCall obj = new btnCall();
                obj.User(txtUserName.Text);
                this.Hide();
                obj.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrect Username or Password");
                txtUserName.Text = "";
                txtPassword.Text = "";
            }
        }

        private void lblCreate_Click(object sender, EventArgs e)
        {
            btnLogin.Visible = false;
            btnRegister.Visible = true;
            lblLogin.Visible = true;
            lblNotRegister.Visible = false;
            lblCreate.Visible = false;

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if(txtPassword.Text!="" && txtUserName.Text != "")
            {
                obj.ExcecuteNonQuery("Insert into LoginTable (UserName, Password) Values('" + txtUserName.Text + "','" + txtPassword.Text + "')");
                txtPassword.Text = "";
                txtUserName.Text = "";
                btnRegister.Visible = false;
                btnLogin.Visible = true;
                lblLogin.Visible = false;
            }
            

        }

        private void lblLogin_Click(object sender, EventArgs e)
        {
            txtPassword.Text = "";
            txtUserName.Text = "";
            btnRegister.Visible = false;
            btnLogin.Visible = true;
            lblLogin.Visible = false;
        }
    }
}