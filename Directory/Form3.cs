﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Directory
{
    public partial class Form3 : System.Windows.Forms.Form
    {
        MyDb obj = new MyDb();
        string userName;
        string Company;
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            
        }
        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string type;
            try
            {
                if (rbtnPublic.Checked)
                {
                    type = "Public";
                }
                else
                {
                    type = "Personal";
                }
                obj.InsertContact("Insert into ContactsTable (Company, Contact1, Pos1, PhoneNo1, Contact2, Pos2, PhoneNo2, Contact3, Pos3, PhoneNo3, Contact4, Pos4, PhoneNo4, Contact5, Pos5, PhoneNo5, Contact6, Pos6, PhoneNo6, Contact7, Pos7, PhoneNo7, Contact8, Pos8, PhoneNo8, Contact9, Pos9, PhoneNo9, Contact10, Pos10, PhoneNo10, UserName, Type, Email1, Email2, Email3, Email4, Email5, Email6, Email7, Email8, Email9, Email10) values( '" + txtCompany.Text + "','" + txtName1.Text + "', '" + txtDes1.Text + "', '" + txtPhoneNo1.Text + "', '" + txtName2.Text + "','" + txtDes2.Text + "', '" + txtPhoneNo2.Text + "' , '" + txtName3.Text + "' , '" + txtDes3.Text + "','" + txtPhoneNo3.Text + "', '" + txtName4.Text + "','" + txtDes4.Text + "', '" + txtPhoneNo4.Text + "', '" + txtName5.Text + "','" + txtDes5.Text + "', '" + txtPhoneNo5.Text + "', '" + txtName6.Text + "','" + txtDes6.Text + "', '" + txtPhoneNo6.Text + "', '" + txtName7.Text + "','" + txtDes7.Text + "', '" + txtPhoneNo7.Text + "', '" + txtName8.Text + "','" + txtDes8.Text + "', '" + txtPhoneNo8.Text + "', '" + txtName9.Text + "','" + txtDes9.Text + "', '" + txtPhoneNo9.Text + "', '" + txtName10.Text + "','" + txtDes10.Text + "', '" + txtPhoneNo10.Text + "', '" + userName + "', '" + type + "','" + txtEmail1.Text + "','" + txtEmail2.Text + "','" + txtEmail3.Text + "','" + txtEmail4.Text + "','" + txtEmail5.Text + "','" + txtEmail6.Text + "','" + txtEmail7.Text + "','" + txtEmail8.Text + "','" + txtEmail9.Text + "','" + txtEmail10.Text + "')");
                clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        public void clear()
        {
            foreach (TextBox textBox in Controls.OfType<TextBox>())
                textBox.Text = "";
        }
        public void User(string user)
        {
            this.userName = user;
        }

        public void display(string company)
        {                             
            this.Company = company;
            DataTable dT = obj.GetDataTable("Select * from ContactsTable where Company='" + company + "'");
            try
            {
                btnSave.Visible = true;
                btnAdd.Visible = false;
                btnDelete.Visible = true;
                txtCompany.Text = dT.Rows[0][0].ToString().Trim();
                txtName1.Text = dT.Rows[0][1].ToString().Trim();
                txtDes1.Text = dT.Rows[0][2].ToString().Trim();
                txtPhoneNo1.Text = dT.Rows[0][3].ToString().Trim();
                txtEmail1.Text = dT.Rows[0][4].ToString().Trim();

                txtName2.Text = dT.Rows[0][5].ToString().Trim();
                txtDes2.Text = dT.Rows[0][6].ToString().Trim();
                txtPhoneNo2.Text = dT.Rows[0][7].ToString().Trim();
                txtEmail2.Text = dT.Rows[0][8].ToString().Trim();

                txtName3.Text = dT.Rows[0][9].ToString().Trim();
                txtDes3.Text = dT.Rows[0][10].ToString().Trim();
                txtPhoneNo3.Text = dT.Rows[0][11].ToString().Trim();
                txtEmail3.Text = dT.Rows[0][12].ToString().Trim();

                txtName4.Text = dT.Rows[0][13].ToString().Trim();
                txtDes4.Text = dT.Rows[0][14].ToString().Trim();
                txtPhoneNo4.Text = dT.Rows[0][15].ToString().Trim();
                txtEmail4.Text = dT.Rows[0][16].ToString().Trim();

                txtName5.Text = dT.Rows[0][17].ToString().Trim();
                txtDes5.Text = dT.Rows[0][18].ToString().Trim();
                txtPhoneNo5.Text = dT.Rows[0][19].ToString().Trim();
                txtEmail5.Text = dT.Rows[0][20].ToString().Trim();

                txtName6.Text = dT.Rows[0][21].ToString().Trim();
                txtDes6.Text = dT.Rows[0][22].ToString().Trim();
                txtPhoneNo6.Text = dT.Rows[0][23].ToString().Trim();
                txtEmail6.Text = dT.Rows[0][24].ToString().Trim();

                txtName7.Text = dT.Rows[0][25].ToString().Trim();
                txtDes7.Text = dT.Rows[0][26].ToString().Trim();
                txtPhoneNo7.Text = dT.Rows[0][27].ToString().Trim();
                txtEmail7.Text = dT.Rows[0][28].ToString().Trim();

                txtName8.Text = dT.Rows[0][29].ToString().Trim();
                txtDes8.Text = dT.Rows[0][30].ToString().Trim();
                txtPhoneNo8.Text = dT.Rows[0][31].ToString().Trim();
                txtEmail8.Text = dT.Rows[0][32].ToString().Trim();

                txtName9.Text = dT.Rows[0][33].ToString().Trim();
                txtDes9.Text = dT.Rows[0][34].ToString().Trim();
                txtPhoneNo9.Text = dT.Rows[0][35].ToString().Trim();
                txtEmail9.Text = dT.Rows[0][36].ToString().Trim();

                txtName10.Text = dT.Rows[0][37].ToString().Trim();
                txtDes10.Text = dT.Rows[0][38].ToString().Trim();
                txtPhoneNo10.Text = dT.Rows[0][39].ToString().Trim();
                txtEmail10.Text = dT.Rows[0][40].ToString().Trim();

                if (dT.Rows[0][32].ToString().Trim() == "Public")
                {
                    rbtnPublic.Checked = true;
                }
                else
                {
                    rbtnPersonal.Checked = true;
                }
                
                btnCall1.Visible = true;
                btnCall2.Visible = true;
                btnCall3.Visible = true;
                btnCall4.Visible = true;
                btnCall5.Visible = true;
                btnCall6.Visible = true;
                btnCall7.Visible = true;
                btnCall8.Visible = true;
                btnCall9.Visible = true;
                btnCall10.Visible = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string type;

            if (rbtnPublic.Checked)
            {
                type = "Public";
            }
            else
            {
                type = "Personal";
            }

            obj.InsertContact("Update ContactsTable set Company='" + txtCompany.Text + "', Contact1='" + txtName1.Text + "', Pos1= '" + txtDes1.Text + "', PhoneNo1= '" + txtPhoneNo1.Text + "', Contact2= '" + txtName2.Text + "', Pos2='" + txtDes2.Text + "',  PhoneNo2='" + txtPhoneNo2.Text + "' , Contact3= '" + txtName3.Text + "' , Pos3= '" + txtDes3.Text + "', PhoneNo3='" + txtPhoneNo3.Text + "', Contact4= '" + txtName4.Text + "', Pos4='" + txtDes4.Text + "', PhoneNo4= '" + txtPhoneNo4.Text + "', Contact5= '" + txtName5.Text + "', Pos5='" + txtDes5.Text + "', PhoneNo5= '" + txtPhoneNo5.Text + "', Contact6= '" + txtName6.Text + "', Pos6='" + txtDes6.Text + "', PhoneNo6= '" + txtPhoneNo6.Text + "', Contact7= '" + txtName7.Text + "', Pos7='" + txtDes7.Text + "', PhoneNo7= '" + txtPhoneNo7.Text + "', Contact8= '" + txtName8.Text + "', Pos8='" + txtDes8.Text + "', PhoneNo8= '" + txtPhoneNo8.Text + "',  Contact9='" + txtName9.Text + "', Pos9='" + txtDes9.Text + "', PhoneNo9= '" + txtPhoneNo9.Text + "', Contact10= '" + txtName10.Text + "', Pos10='" + txtDes10.Text + "', PhoneNo10= '" + txtPhoneNo10.Text + "', Type='" + type + "', Email1='" + txtEmail1.Text + "', Email2='" + txtEmail2.Text + "', Email3='" + txtEmail3.Text + "', Email4='" + txtEmail4.Text + "', Email5='" + txtEmail5.Text + "', Email6='" + txtEmail6.Text + "', Email7='" + txtEmail7.Text + "', Email8='" + txtEmail8.Text + "', Email9='" + txtEmail9.Text + "', Email10='" + txtEmail10.Text + "' where UserName='" + userName + "' and Company='" + Company + "' ");
            clear();

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            obj.ExcecuteNonQuery("Delete from ContactsTable where Company='" + Company + "' and UserName='" + userName + "'");
            clear();
            this.Hide();
        }
    }
}
