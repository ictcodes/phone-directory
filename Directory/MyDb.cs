﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Directory
{
    class MyDb
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConn"].ToString());
        public void InsertContact(string query)
        {
            SqlCommand cmd;
            
            cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            
            con.Open();
            cmd.ExecuteNonQuery();

            DataSet ds = new DataSet();
            con.Close();
        }
        public DataTable getContacts(string userName)
        {
            SqlCommand cmd = new SqlCommand("select Company,Contact1,PhoneNo1,Contact2,PhoneNo2,Contact3,PhoneNo3,Contact4,PhoneNo4,Contact5,PhoneNo5,Contact6,PhoneNo6,Contact7,PhoneNo7,Contact8,PhoneNo8,Contact9,PhoneNo9,Contact10,PhoneNo10 from ContactsTable where Type='Public' or UserName='" + userName + "' ", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            con.Close();

            return dt;
        }
        public bool login(string userName, string password)
        {
            SqlCommand cmd = new SqlCommand("select * from LoginTable where UserName='" + userName + "' and Password='" + password + "'", con);
             con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public DataTable GetDataTable(string strquery)
        {
            SqlCommand command = new SqlCommand(strquery, con);
            SqlDataAdapter ODA = new SqlDataAdapter(command);
            try
            {
                DataTable dtTemp = new DataTable();
                ODA.Fill(dtTemp);

                return dtTemp;
            }
            catch (Exception ex) { return null; }

            finally
            {
                con.Close();
            }

        }

        public void ExcecuteNonQuery(string strquery)
        {
            try
            {
                con.Open();

            }
            catch (Exception ex)
            {
                
            }

            SqlCommand OSC = new SqlCommand(strquery, con);
            OSC.ExecuteNonQuery();
            con.Close();

        }

    }
}
